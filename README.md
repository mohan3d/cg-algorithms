# CG-Algorithms
A well-designed Computational Geometry library to be used for implementing convex hull algorithms, segment intersection algorithms, triangulation, e.t.c.

**The original repository can be found [here](https://bitbucket.org/ASU_CG/cg).**

# Implemented Algorithms

* Convex Hull
    * Extreme Points
    * Extreme Segments
    * Jarvis March
    * Graham Scan
    * Incremental Algorithm
    * Divide & Conquer
    * Quick Hull
* Segment Intersection
    * Sweep Line
* Triangulation
    * Inserting Diagonals
    * Subtracting Ears
    * Monotone Partitioning & Monotone Triangulation
