﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CGAlgorithms.Algorithms.ConvexHull;
using CGAlgorithms;
using CGUtilities;
using System.Collections.Generic;


namespace CGAlgorithmsUnitTest
{
    [TestClass]
    public class QuickHullThreadedTest : ConvexHullTest
    {
        [TestMethod]
        public void QuickHullNormalTestCase20Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case20Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase40Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case40Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase60Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case60Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase80Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case80Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase100Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case100Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase200Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case200Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase400Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case400Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase600Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case600Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase800Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case800Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase1000Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case1000Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase2000Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case2000Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase3000Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case3000Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase4000Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case4000Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase5000Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case5000Points();
        }
        [TestMethod]
        public void QuickHullNormalTestCase10000Points()
        {
            convexHullTester = new QuickHullThreaded();
            Case10000Points();
        }
        [TestMethod]
        public void QuickHullSpecialCase10SamePoints()
        {
            convexHullTester = new QuickHullThreaded();
            SpecialCase10SamePoints();
        }
        [TestMethod]
        public void QuickHullSpecialCaseLine()
        {
            convexHullTester = new QuickHullThreaded();
            SpecialCaseLine();
        }
        [TestMethod]
        public void QuickHullSpecialCaseTriangle()
        {
            convexHullTester = new QuickHullThreaded();
            SpecialCaseTriangle();
        }
        [TestMethod]
        public void QuickHullSpecialCaseCircle()
        {
            convexHullTester = new QuickHullThreaded();
            SpecialCaseCircle();
        }
        [TestMethod]
        public void QuickHullSpecialCaseConvexPolygon()
        {
            convexHullTester = new QuickHullThreaded();
            SpecialCaseConvexPolygon();
        }
    }
}
