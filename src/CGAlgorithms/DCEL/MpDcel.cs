﻿using System.Collections.Generic;
using CGUtilities;

namespace CGAlgorithms.DCEL
{
    class MpDcel : Dcel
    {

        public void AddEdge(Vertex a, Vertex b)
        {
            if (a.IncidentEdge.IncidentFace != b.IncidentEdge.IncidentFace)
            {
                NonDirectAddEdge(a, b);
            }
            else
            {
                DirectAddEdge(a, b);
            }
        }

        private void DirectAddEdge(Vertex a, Vertex b)
        {
            var firstEdge = a.IncidentEdge;
            var secondEdge = b.IncidentEdge;
            AddEdge(ref firstEdge, ref secondEdge);
        }


        private void NonDirectAddEdge(Vertex a, Vertex b)
        {
            // Get first, second edges sharing same face.
            // call add edge with them.
            var edges = EdgesInSharedFace(a, b);
            var firstEdge = edges.Key;
            var secondEdge = edges.Value;

            AddEdge(ref firstEdge, ref secondEdge);

        }

        private void AddEdge(ref HalfEdge firstEdge, ref HalfEdge secondEdge)
        {
            if (firstEdge == null || secondEdge == null) return;

            // Delete current face, (first and second edges share the same face)
            var currentFace = firstEdge.IncidentFace;
            Faces.Remove(currentFace);

            // We have 2 faces now 
            var face1 = new Face();
            var face2 = new Face();
            
            // 2 half edges for new edge.
            var e = new HalfEdge { Origin = firstEdge.Origin };
            var eTwin = new HalfEdge { Origin = secondEdge.Origin };


            // First face.
            var secondEdgePrevious = secondEdge.Previous;
            e.SetNext(ref secondEdge);
            // e.SetPrevious(firstEdge.Previous).
            firstEdge.Previous.SetNext(ref e);
            // Set face1 Incident edge to e.
            face1.Edge = e;
            // Update face of edges in face1.
            var tmp = e;
            do
            {
                tmp.IncidentFace = face1;
                tmp = tmp.Next;
            } while (tmp != e);

            // Update faces, edges.
            Edges.AddLast(e);
            Faces.AddLast(face1);
            

            // Second face.
            eTwin.SetNext(ref firstEdge);
            secondEdgePrevious.SetNext(ref eTwin);
            face2.Edge = eTwin;
            //eTwin.IncidentFace = face2;
            // Update faces !
            tmp = eTwin;
            do
            {
                tmp.IncidentFace = face2;
                tmp = tmp.Next;
            } while (tmp != eTwin);

            e.SetTwin(ref eTwin);
            Edges.AddLast(eTwin);
            Faces.AddLast(face2);
        }

        public List<Polygon> SubPolygons()
        {
            var polygons = new List<Polygon>();

            foreach (var face in Faces)
            {
                if(face.OutterFace) continue;

                // List of edges.
                var edges = new List<Line>();

                HalfEdge prev = null;
                var e = face.Edge;

                do
                {
                    if (prev != null)
                    {
                        edges.Add(new Line(prev.Origin, e.Origin));
                        prev = e;

                    }
                    else
                    {
                        prev = e;
                    }

                    e = e.Next;

                } while (e != face.Edge);

                // Last edge
                edges.Add(new Line(face.Edge.Previous.Origin, face.Edge.Origin));

                polygons.Add(new Polygon(edges));

            }

            return polygons;
        } 

    }
}
