﻿namespace CGAlgorithms.DCEL
{
    class HalfEdge
    {
        public Vertex Origin { get; set; }
        public HalfEdge Twin { get; set; }
        public Face IncidentFace { get; set; }
        public HalfEdge Next { get; set; }
        public HalfEdge Previous { get; set; }

        public HalfEdge()
        {
            Origin = null;
            Twin = null;
            IncidentFace = null;
            Next = null;
            Previous = null;
        }

        public void SetTwin(ref HalfEdge twinEdge)
        {
            Twin = twinEdge;
            twinEdge.Twin = this;
        }

        public void SetNext(ref HalfEdge nextEdge)
        {
            Next = nextEdge;
            nextEdge.Previous = this;
        }

        public void SetPrevious(ref HalfEdge previousEdge)
        {
            Previous = previousEdge;
            previousEdge.Next = this;
        }

        public override string ToString()
        {
            return "My Origin : " + Origin;
        }
    }
}
