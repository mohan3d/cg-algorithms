﻿using System;
using System.Collections.Generic;
using CGAlgorithms.Algorithms.PolygonTriangulation;

namespace CGAlgorithms.DCEL
{
    class Dcel
    {
        public LinkedList<Vertex> Vertices;
        public LinkedList<HalfEdge> Edges;
        public LinkedList<Face> Faces;
 
        public Dcel()
        {
            Vertices = new LinkedList<Vertex>();
            Edges = new LinkedList<HalfEdge>();
            Faces = new LinkedList<Face>();
        }

        public void CreateDcel<T> (List<T> lv) where T : Vertex
        {
            var f1 = new Face();
            var f2 = new Face();

            HalfEdge prev = null;
            HalfEdge next = null;

            for (var i = 0; i < lv.Count; ++i)
            {
                var first = lv[i];
                var second = lv[(i + 1) % lv.Count];

                var e = new HalfEdge();
                var eTwin = new HalfEdge();

                e.Origin = first;
                eTwin.Origin = second;
                e.IncidentFace = f1;
                eTwin.IncidentFace = f2;
                e.SetTwin(ref eTwin);

                first.IncidentEdge = e;
                //second.IncidentEdge = eTwin;

                if (prev != null && next != null)
                {
                    e.SetPrevious(ref prev);
                    e.Twin.SetNext(ref next);
                }

                prev = e;
                next = eTwin;

                Edges.AddLast(e);
                Edges.AddLast(eTwin);
                Vertices.AddLast(first);
            }

            // Last - 1
            var end = Edges.Last.Value.Twin;
            // First
            var start = Edges.First.Value;

            end.SetNext(ref start);

            // Last
            var t1 = end.Twin;
            // Start + 1
            var t2 = start.Twin;

            t2.SetNext(ref t1);


            // Set face edge
            f1.Edge = start;
            f2.Edge = start.Twin;

            // To be marked as outterface.
            f2.OutterFace = true;

            Faces.AddLast(f1);
            Faces.AddLast(f2);
        }


        public void TraverseFace(Face f, Action<string> output)
        {

            var edge = f.Edge;
            var it = edge;

            do
            {
                output(it.Origin.ToString());
                it = it.Next;

            } while (it != edge);
        }

        public Vertex Target(HalfEdge e)
        {
            return e.Next.Origin;
        }

        public KeyValuePair<HalfEdge, HalfEdge> EdgesInSharedFace(Vertex a, Vertex b)
        {
            var startEdge1 = a.IncidentEdge;
            var startEdge2 = b.IncidentEdge;
            var e1 = a.IncidentEdge;
            var e2 = b.IncidentEdge;

            do
            {
                var f1 = e1.IncidentFace;
                
                do
                {
                    var f2 = e2.IncidentFace;

                    if(f1 == f2 && !f1.OutterFace)
                        return new KeyValuePair<HalfEdge, HalfEdge>(e1, e2);

                    e2 = e2.Twin.Next;
                } while (e2 != startEdge2);
                
                e1 = e1.Twin.Next;

            } while (e1 != startEdge1);

            return new KeyValuePair<HalfEdge, HalfEdge>(null, null);
        } 
    }
}
