﻿namespace CGAlgorithms.DCEL
{
    class Face
    {
        public HalfEdge Edge { get; set; }
        public bool OutterFace { get; set; }
        public Face(HalfEdge edge)
        {
            Edge = edge;
            OutterFace = false;
        }

        public Face() : this(null)
        {
        }

        public override string ToString()
        {
            return "My edge => " + Edge;
        }
    }
}
