﻿using CGUtilities;

namespace CGAlgorithms.DCEL
{
    class Vertex : Point
    {
        public HalfEdge IncidentEdge { get; set; }

        public Vertex(double x, double y)
            : this(x, y, null)
        {
        }

        public Vertex(double x, double y, HalfEdge incidentEdge)
            : base(x, y)
        {
            IncidentEdge = incidentEdge;
        }

        public override string ToString()
        {            
            return "(" + X + ", " + Y + ")";
        }
    }
}
