﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using CGUtilities;



namespace CGAlgorithms.Algorithms.PolygonTriangulation
{
    class SubtractingEars : Algorithm
    {
        public List<Point> PolygonPoints;
        public List<Point> ConvexPoints;
        public List<Point> ReflexPoints;
        public List<Point> EarPoints;
        public List<Line> OutputLines;

        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons,
            ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            foreach (var polygon in polygons)
            {
                InitializeLists();
                GetPolygonPoints(polygon.lines);
                ApplyEarAlgorithm();
                outLines.AddRange(OutputLines.GetRange(0, OutputLines.Count));
            }
        }

        public void InitializeLists()
        {
            PolygonPoints = new List<Point>();
            ConvexPoints = new List<Point>();
            ReflexPoints = new List<Point>();
            EarPoints = new List<Point>();
            OutputLines = new List<Line>();
        }

        public void GetPolygonPoints(List<Line> lines)
        {
            foreach (var line in lines)
            {
                PolygonPoints.Add(line.Start);
            }
        }

        public void CategorizePoints()
        {
            var n = PolygonPoints.Count;
            // O(n^2)
            for (var i = 0; i < PolygonPoints.Count; ++i)
            {
                // Left Turn
                if (HelperMethods.CheckTurn(new Line(PolygonPoints[Prev(i, n)], PolygonPoints[i]), PolygonPoints[Next(i, n)]) ==
                    Enums.TurnType.Left)
                {
                    ConvexPoints.Add(PolygonPoints[i]);

                    if (IsEar(PolygonPoints[i]))
                    {
                        EarPoints.Add(PolygonPoints[i]);
                    }
                }
                else
                {
                    // Assume only right, no colinear
                    ReflexPoints.Add(PolygonPoints[i]);
                }
            }
        }

        public void ApplyEarAlgorithm()
        {
            CategorizePoints();

            while (EarPoints.Count > 0 && PolygonPoints.Count > 3)
            {
                var currentEar = EarPoints[0];
                var earIndex = PolygonPoints.FindIndex(e => e.Equals(currentEar));

                var nextPoint = PolygonPoints[Next(earIndex, PolygonPoints.Count)];
                var prevPoint = PolygonPoints[Prev(earIndex, PolygonPoints.Count)];
                OutputLines.Add(new Line(prevPoint, nextPoint));

                // Ear is convex point, remove from earList, polygon and from convexList.
                PolygonPoints.RemoveAt(earIndex);
                //EarPoints.RemoveAt(earIndex);
                EarPoints.RemoveAt(EarPoints.FindIndex(e => e.Equals(currentEar)));
                ConvexPoints.RemoveAt(ConvexPoints.FindIndex(e => e.Equals(currentEar)));

                CheckPoint(prevPoint);
                CheckPoint(nextPoint);
            }
        }

        public void CheckPoint(Point p)
        {
            if (EarPoints.FindIndex(e => e.Equals(p)) != -1)
            {
                if (!IsEar(p))
                {
                    EarPoints.RemoveAt(EarPoints.FindIndex(e => e.Equals(p)));
                }
            }
            else if (ConvexPoints.FindIndex(e => e.Equals(p)) != -1)
            {
                // may be it was convex but not Ear, check if it's now ear.
                if (IsEar(p))
                {
                    EarPoints.Add(p);
                }
            }
            else if (ReflexPoints.FindIndex(e => e.Equals(p)) != -1)
            {
                var pIndex = PolygonPoints.FindIndex(e => e.Equals(p));
                var prev = Prev(pIndex, PolygonPoints.Count);
                var next = Next(pIndex, PolygonPoints.Count);

                if (HelperMethods.CheckTurn(new Line(PolygonPoints[prev], p), PolygonPoints[next]) ==
                    Enums.TurnType.Left)
                {
                    // Became convex
                    ConvexPoints.Add(p);
                    ReflexPoints.RemoveAt(ReflexPoints.FindIndex(e => e.Equals(p)));

                    // Maybe it's Ear now
                    if (IsEar(p))
                    {
                        EarPoints.Insert(0, p);
                        //EarPoints.Add(p);
                    }
                }
            }
        }

        public bool IsEar(Point p)
        {
            var n = PolygonPoints.Count;
            var i = PolygonPoints.FindIndex(e => e.Equals(p));

            // Check if it's ear
            for (var j = Next(Next(i, n), n); j != Prev(i, n); j = Next(j, n))
            {
                if (HelperMethods.PointInTriangle(PolygonPoints[j], PolygonPoints[Prev(i, n)], PolygonPoints[i],
                        PolygonPoints[Next(i, n)]) == Enums.PointInPolygon.Inside)
                {
                    return false;
                }
            }

            return true;
        }

        public int Next(int i, int n)
        {
            return (i + 1) % n;
        }

        public int Prev(int i, int n)
        {
            return ((i - 1 >= 0) ? (i - 1) : (n - 1));
        }
        public override string ToString()
        {
            return "Subtracting Ears";
        }
    }
}
