﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGUtilities;
using CGUtilities.DataStructures;

namespace CGAlgorithms.Algorithms.PolygonTriangulation
{
    class InsertingDiagonals : Algorithm
    {
        public List<Line> OutPutLines;

        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons,
            ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            foreach (var polygon in polygons)
            {
                OutPutLines = new List<Line>();
                RunAlgorithm(polygon.lines.ConvertAll(e => e.Start));
                outLines.AddRange(OutPutLines.GetRange(0, OutPutLines.Count));
            }
        }

        void RunAlgorithm(List<Point> points)
        {
            if (points.Count <= 3)
            {
                return;
            }

            var splitedLists = SplitPoints(points);

            RunAlgorithm(splitedLists.First);
            RunAlgorithm(splitedLists.Second);
        }
        public Line FindDiagonal(List<Point> points)
        {
            var n = points.Count;
            var minXPoint = points.Aggregate((a, b) => (a.X < b.X) ? a : b);
            var minXIndex = points.IndexOf(minXPoint);

            var nextIndex = Next(minXIndex, n);
            var prevIndex = Prev(minXIndex, n);

            var isEar = true;
            var s = prevIndex;

            //for (var i = 0; i < points.Count; ++i)
            for (var i = Next(nextIndex, n); i != prevIndex; i = Next(i, n))
            {                
                if (HelperMethods.PointInTriangle(points[i], points[prevIndex], minXPoint, points[nextIndex]) == Enums.PointInPolygon.Inside)
                {
                    isEar = false;
                    if (Area(points[i], points[nextIndex], points[prevIndex]) > Area(points[s], points[nextIndex], points[prevIndex]))
                    {
                        s = i;
                    }
                }
            }

            return ((isEar) ? (new Line(points[prevIndex], points[nextIndex])) : (new Line(minXPoint, points[s])));
        }

        public Pair<List<Point>, List<Point>> SplitPoints(List<Point> points)
        {
            var firstList = new List<Point>();
            var secondList = new List<Point>();

            var diagonal = FindDiagonal(points);
            OutPutLines.Add(diagonal);

            var n = points.Count;
            var startIndex = points.FindIndex(e => e.Equals(diagonal.Start));
            var endIndex = points.FindIndex(e => e.Equals(diagonal.End));

            for (var i = endIndex/*Next(endIndex, n)*/; i != startIndex; i = Next(i, n))
            {
                firstList.Add(points[i]);
            }
            firstList.Add(points[startIndex]);

            for (var i = endIndex/*Prev(endIndex, n)*/; i != startIndex; i = Prev(i, n))
            {
                secondList.Add(points[i]);
            }
            secondList.Add(points[startIndex]);

            return new Pair<List<Point>, List<Point>>(firstList, secondList);
        }

        public double Area(Point a, Point b, Point c)
        {
            return (0.5 * Math.Abs(((b.X - a.X) * (c.Y - b.Y)) - ((b.X - c.X) * (a.Y - b.Y))));
        }

        public int Next(int i, int n)
        {
            return (i + 1) % n;
        }

        public int Prev(int i, int n)
        {
            return ((i - 1 >= 0) ? (i - 1) : (n - 1));
        }

        public override string ToString()
        {
            return "Inserting Diagonals";
        }
    }
}
