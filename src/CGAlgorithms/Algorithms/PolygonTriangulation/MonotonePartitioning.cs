﻿using System;
using System.Collections.Generic;
using System.Linq;
using CGUtilities;
using CGUtilities.DataStructures;
using CGAlgorithms.DCEL;

namespace CGAlgorithms.Algorithms.PolygonTriangulation
{
    class MonotonePartitioning : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons,
            ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            if (polygons.Count == 0) return;

            Initialize();
            MakeMonotone(polygons[0]);
            outLines.AddRange(D.GetRange(0, D.Count));
            outPolygons.AddRange(DcelDs.SubPolygons());
        }

        public OrderedSet<MonoLine> T;
        public List<Line> D;
        public List<MonoLine> M;
        public List<Polygon> SubPolygons;
        public MpDcel DcelDs;


        public void MakeMonotone(Polygon polygon)
        {
            Initialize();

            var monoPoints = CategorizePoints(polygon.lines.ConvertAll(e => e.Start));

            DcelDs.CreateDcel(monoPoints.ToList());


            for (var i = 0; i < monoPoints.Count; i++)
            {
                monoPoints[i].NextPoint = monoPoints[Next(i, monoPoints.Count)];
            }



            M = monoPoints.Select((t, i) => new MonoLine(t, monoPoints[Next(i, monoPoints.Count)])).ToList();
            monoPoints.Sort();

            monoPoints.ForEach(HandlePoint);
        }

        public void Initialize()
        {
            T = new OrderedSet<MonoLine>();
            D = new List<Line>();
            M = new List<MonoLine>();
            SubPolygons = new List<Polygon>();
            DcelDs = new MpDcel();
            //RPoint = null;
        }
        public class MonoPoint : /*Point*/ Vertex, IComparable<MonoPoint>
        {
            public enum MonoPointType
            {
                Start,                  // Initial
                End,                    // Final
                Split,                  // Local minimum
                Merge,                  // Local maximum
                Regular,                // Passing
                Colinear                // Colinear
            }

            public MonoPointType Type { get; set; }
            public int EdgeIndex { get; set; }

            public MonoPoint NextPoint { get; set; }


            public MonoPoint(Point p, MonoPointType type, int edgeIndex)
                : base(p.X, p.Y)
            {
                Type = type;
                EdgeIndex = edgeIndex;
            }

            public MonoPoint(double x, double y, MonoPointType type, int edgeIndex)
                : this(new Point(x, y), type, edgeIndex)
            { }

            public Point GetPoint()
            {
                return new Point(X, Y);
            }

            // Max y and min x in tie
            public int CompareTo(MonoPoint other)
            {
                if (SameValue(Y, other.Y))
                {
                    // same y, min x
                    if (X < other.X)
                        return -1;
                    if (X > other.X)
                        return 1;
                }

                if (Y > other.Y)
                    return -1;

                return Y < other.Y ? 1 : 0;
            }

            public override bool Equals(object obj)
            {
                var other = (MonoPoint)obj;
                return base.Equals(obj) && (Type == other.Type);
            }

            public override string ToString()
            {
                return "(" + X + ", " + Y + ", " + Type + ")";
            }
        }
        public class MonoLine : IComparable<MonoLine>
        {
            public MonoPoint FirstPoint { get; set; }
            public MonoPoint SecondPoint { get; set; }
            public MonoPoint HelperPoint { get; set; }

            public static double CurrentY;

            public Line GetLine()
            {
                // Return line with Start point (having max y), End point (having min y)
                return FirstPoint.Y > SecondPoint.Y ? new Line(FirstPoint.GetPoint(), SecondPoint.GetPoint()) : new Line(SecondPoint.GetPoint(), FirstPoint.GetPoint());
            }
            public Line GetOriginalLine()
            {
                return new Line(FirstPoint.GetPoint(), SecondPoint.GetPoint());
            }

            public MonoLine(MonoPoint startPoint, MonoPoint endPoint, MonoPoint helperPoint)
            {
                FirstPoint = startPoint;
                SecondPoint = endPoint;
                HelperPoint = helperPoint;
            }
            public MonoLine(MonoPoint startPoint, MonoPoint endPoint) :
                this(startPoint, endPoint, startPoint) { }


            public int CompareTo(MonoLine other)
            {

                var sweepline = SweepLine();
                var ix = IntersectionX(this, sweepline);
                var otherIx = IntersectionX(other, sweepline);

                if (ix < otherIx)
                {
                    return -1;
                }

                return ix > otherIx ? 1 : 0;
            }

            public override bool Equals(object obj)
            {
                var other = (MonoLine)obj;

                return (FirstPoint.Equals(other.FirstPoint) && SecondPoint.Equals(other.SecondPoint) /*&&
                        HelperPoint.Equals(other.HelperPoint)*/);
            }

            public static double IntersectionX(MonoLine l1, MonoLine l2)
            {

                var a = (l1.FirstPoint.Y - l1.SecondPoint.Y) / (l1.FirstPoint.X - l1.SecondPoint.X);
                var c = l1.FirstPoint.Y - (a * l1.FirstPoint.X);

                var b = (l2.FirstPoint.Y - l2.SecondPoint.Y) / (l2.FirstPoint.X - l2.SecondPoint.X);
                var d = l2.FirstPoint.Y - (b * l2.FirstPoint.X);

                var x = (d - c) / (a - b);
                return x;
            }

            public static MonoLine SweepLine()
            {
                // Regular, -1 , will not be used at all.
                var start = new MonoPoint(int.MaxValue, CurrentY, MonoPoint.MonoPointType.Regular, -1);
                var end = new MonoPoint(int.MinValue, CurrentY, MonoPoint.MonoPointType.Regular, -1);

                return new MonoLine(start, end);
            }
            public override string ToString()
            {
                return "First Point: " + FirstPoint + "\n"
                      + "Second Point: " + SecondPoint + "\n"
                      + "Helper Point: " + HelperPoint;
            }
        }
        // Tested.
        public List<MonoPoint> CategorizePoints(List<Point> points)
        {
            // If not ccw , reverse.
            // Assume no co-linear points.
            var sum = 0.0;
            for (var i = 0; i < points.Count; ++i)
            {
                var p = points[i];
                var nextP = points[Next(i, points.Count)];

                sum += ((nextP.X - p.X) * (nextP.Y + p.Y));
            }
            // CW
            if (sum > 0)
            {
                points.Reverse();
            }

            var monoPoints = new List<MonoPoint>();
            var n = points.Count;

            for (var i = 0; i < n; ++i)
            {
                var p = points[i];
                var next = points[Next(i, n)];
                var prev = points[Prev(i, n)];

                if (SameY(prev, p) && SameY(next, p))
                    monoPoints.Add(new MonoPoint(p, MonoPoint.MonoPointType.Colinear, i));

                else if (Less_yx(prev, p))
                {
                    if (Less_yx(next, p))
                    {
                        // Both prev, next below p 

                        if (HelperMethods.CheckTurn(new Line(prev, p), next) == Enums.TurnType.Left)
                        {
                            // Both prev, next below p && left turn.
                            monoPoints.Add(new MonoPoint(p, MonoPoint.MonoPointType.Start, i));
                        }
                        else
                        {
                            // Both prev, next below p && right turn.
                            monoPoints.Add(new MonoPoint(p, MonoPoint.MonoPointType.Split, i));
                        }
                    }
                    else
                    {
                        // Prev below p but next above p.
                        monoPoints.Add(new MonoPoint(p, MonoPoint.MonoPointType.Regular, i));
                    }
                }
                else
                {
                    // prev above p
                    if (Less_yx(p, next))
                    {
                        // both prev, next above p
                        if (HelperMethods.CheckTurn(new Line(prev, p), next) == Enums.TurnType.Left)
                        {
                            // Both prev, next below p && left turn.
                            monoPoints.Add(new MonoPoint(p, MonoPoint.MonoPointType.End, i));
                        }
                        else
                        {
                            // Both prev, next below p && right turn.
                            monoPoints.Add(new MonoPoint(p, MonoPoint.MonoPointType.Merge, i));
                        }
                    }
                    else
                    {
                        // Prev abobe p but next below p.
                        monoPoints.Add(new MonoPoint(p, MonoPoint.MonoPointType.Regular, i));
                    }
                }
            }

            return monoPoints;
        }
        public void HandlePoint(MonoPoint vi)
        {
            MonoLine.CurrentY = vi.Y;

            switch (vi.Type)
            {
                case MonoPoint.MonoPointType.Start:
                    {
                        HandleStartPoint(vi);
                    }
                    break;

                case MonoPoint.MonoPointType.End:
                    {
                        HandleEndPoint(vi);
                    }
                    break;

                case MonoPoint.MonoPointType.Split:
                    {
                        HandleSplitPoint(vi);
                    }
                    break;
                case MonoPoint.MonoPointType.Merge:
                    {
                        HandleMergePoint(vi);
                    }
                    break;
                case MonoPoint.MonoPointType.Regular:
                    {
                        HandleRegularPoint(vi);
                    }
                    break;
                case MonoPoint.MonoPointType.Colinear:
                    {
                        HandleColinearPoint(vi);
                    }
                    break;
            }
        }
        public MonoLine DirectlyToLeft(MonoPoint vi)
        {
            var l = M[vi.EdgeIndex];
            //var tmp = T.DirectUpperAndLower(l);
            //return T.DirectUpperAndLower(l).Value;
            return T.DirectRightAndLeft(l).Value;
        }
        public void HandleStartPoint(MonoPoint vi)
        {
            var l = M[vi.EdgeIndex];
            T.Add(l);
        }
        public void HandleEndPoint(MonoPoint vi)
        {
            //if (IsMergeVertex(Prev(index, M.Count)))
            // var prevEdge = M[vi.EdgeIndex - 1];
            var prevEdge = M[Prev(vi.EdgeIndex, M.Count)];

            if (HelperIsMergeVertex(prevEdge))
            {
                var helperPoint = prevEdge.HelperPoint;
                //D.Add(new Line(vi, helperPoint.GetPoint()));
                D.Add(new Line(vi, helperPoint));
                DcelDs.AddEdge(vi, helperPoint);
                //CreatePolygon(vi, helperPoint);
            }

            T.Remove(prevEdge);
        }
        public void HandleSplitPoint(MonoPoint vi)
        {
            var l = M[vi.EdgeIndex];

            T.Add(l);
            var lineToLeft = DirectlyToLeft(vi);
            //T.Remove(l);

            if (lineToLeft == null) return;

            //D.Add(new Line(vi, lineToLeft.HelperPoint.GetPoint()));
            D.Add(new Line(vi, lineToLeft.HelperPoint));
            DcelDs.AddEdge(vi, lineToLeft.HelperPoint);
            //CreatePolygon(vi, lineToLeft.HelperPoint);
            T.Remove(lineToLeft);
            lineToLeft.HelperPoint = vi;
            T.Add(lineToLeft);

            //T.Add(l);
        }
        public void HandleMergePoint(MonoPoint vi)
        {
            //var prevEdge = M[vi.EdgeIndex - 1];
            var prevEdge = M[Prev(vi.EdgeIndex, M.Count)];

            if (HelperIsMergeVertex(prevEdge))
            {
                var helperPoint = prevEdge.HelperPoint;
                //D.Add(new Line(vi, helperPoint.GetPoint()));
                D.Add(new Line(vi, helperPoint));
                DcelDs.AddEdge(vi, helperPoint);
                //CreatePolygon(vi, helperPoint);
            }

            T.Remove(prevEdge);

            var ei = M[vi.EdgeIndex];
            T.Add(ei);
            var lineToLeft = DirectlyToLeft(vi);
            T.Remove(ei);

            if (HelperIsMergeVertex(lineToLeft))
            {
                //D.Add(new Line(vi, lineToLeft.HelperPoint.GetPoint()));
                D.Add(new Line(vi, lineToLeft.HelperPoint));
                DcelDs.AddEdge(vi, lineToLeft.HelperPoint);
                //CreatePolygon(vi, lineToLeft.HelperPoint);
            }

            if (lineToLeft == null) return;

            T.Remove(lineToLeft);
            lineToLeft.HelperPoint = vi;
            T.Add(lineToLeft);
        }

        public void HandleRegularPoint(MonoPoint vi)
        {
            var ei = M[vi.EdgeIndex];

            if (InteriorToRight(vi.EdgeIndex))
            {
                // var prevEdge = M[vi.EdgeIndex - 1];
                var prevEdge = M[Prev(vi.EdgeIndex, M.Count)];

                if (HelperIsMergeVertex(prevEdge))
                {
                    //D.Add(new Line(vi, prevEdge.HelperPoint.GetPoint()));
                    D.Add(new Line(vi, prevEdge.HelperPoint));
                    DcelDs.AddEdge(vi, prevEdge.HelperPoint);
                    //CreatePolygon(vi, prevEdge.HelperPoint);
                }

                T.Remove(prevEdge);
                T.Add(ei);
            }
            else
            {
                T.Add(ei);
                var lineToLeft = DirectlyToLeft(vi);
                T.Remove(ei);

                if (HelperIsMergeVertex(lineToLeft))
                {
                    //D.Add(new Line(vi, lineToLeft.HelperPoint.GetPoint()));
                    D.Add(new Line(vi, lineToLeft.HelperPoint));
                    DcelDs.AddEdge(vi, lineToLeft.HelperPoint);
                    //CreatePolygon(vi, lineToLeft.HelperPoint);
                }


                if (lineToLeft == null) return;

                T.Remove(lineToLeft);
                lineToLeft.HelperPoint = vi;
                T.Add(lineToLeft);

            }
        }
        public void HandleColinearPoint(MonoPoint vi)
        {

            var prevEdge = M[Prev(vi.EdgeIndex, M.Count)];

            T.Remove(prevEdge);
            T.Add(M[vi.EdgeIndex]);

        }
        public int Next(int i, int n)
        {
            return (i + 1) % n;
        }
        public int Prev(int i, int n)
        {
            return ((i - 1 >= 0) ? (i - 1) : (n - 1));
        }
        public static bool SameY(Point a, Point b)
        {
            return Math.Abs(a.Y - b.Y) <= Constants.Epsilon;
        }
        public static bool SameX(Point a, Point b)
        {
            return Math.Abs(a.X - b.X) <= Constants.Epsilon;
        }
        public static bool SameValue(double a, double b)
        {
            return Math.Abs(a - b) <= Constants.Epsilon;
        }
        public static bool Less_yx(Point a, Point b)
        {
            // y1 < y2 ? or (y1 == y2) x1 < x2 ?
            if (a.Y < b.Y)
                return true;
            if (a.Y > b.Y)
                return false;

            return a.X < b.X;
        }

        public bool HelperIsMergeVertex(MonoLine l)
        {
            if (!T.Contains(l)) return false;
            return l.HelperPoint.Type == MonoPoint.MonoPointType.Merge;
        }
        public bool InteriorToRight(int index)
        {

            var currentEdge = M[index];
            var prev = M[Prev(index, M.Count)].FirstPoint;
            /*
            return HelperMethods.CheckTurn(new Line(prev, currentEdge.FirstPoint), currentEdge.SecondPoint) ==
                   Enums.TurnType.Left;
            */

            return prev.Y > currentEdge.FirstPoint.Y && currentEdge.FirstPoint.Y > currentEdge.SecondPoint.Y;
            /*return HelperMethods.CheckTurn(new Line(prev, currentEdge.FirstPoint), currentEdge.SecondPoint) ==
                   Enums.TurnType.Left;*/
        }



        public override string ToString()
        {
            return "Monotone Partitioning";
        }
    }
}
