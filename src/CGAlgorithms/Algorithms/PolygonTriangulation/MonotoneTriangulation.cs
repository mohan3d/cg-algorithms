﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGUtilities;

namespace CGAlgorithms.Algorithms.PolygonTriangulation
{
    class MonotoneTriangulation : Algorithm
    {

        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons,
            ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            var ppoints = polygons[0].lines.ConvertAll(e => e.Start);
            OutLines = new List<Line>();

            if (!CheckMonotone(ppoints))
            {
                var mp = new MonotonePartitioning();
                var subPolygons = new List<Polygon>();
                var op = new List<Point>();
                var ol = new List<Line>();

                mp.Run(new List<Point>(), new List<Line>(), polygons, ref op, ref ol, ref subPolygons);

                if (subPolygons.Any(subPolygon => !CheckMonotone(subPolygon.lines.ConvertAll(e => e.Start))))
                {
                    return;
                }

                // Copy diagonals.
                outLines.AddRange(ol.GetRange(0, ol.Count));

                foreach (var subPolygon in subPolygons)
                {
                    MonotoneTriangulate(subPolygon.lines.ConvertAll(e => e.Start));
                }
            }
            else
            {
                MonotoneTriangulate(ppoints);
            }

            outLines.AddRange(OutLines.GetRange(0, OutLines.Count));

        }

        public List<Line> OutLines;

        public void MonotoneTriangulate(List<Point> ppoints)
        {
            var E = PreparePoints(ppoints);
            var S = new Stack<ChainPoint>();

            S.Push(E[0]);
            S.Push(E[1]);

            var i = 2;

            while (i != ppoints.Count)
            {
                var p = E[i];
                var top = S.Peek();

                if (p.Type == top.Type)
                {
                    S.Pop();
                    var top2 = S.Peek();

                    if (IsConvex(top2, top, p))
                    {
                        OutLines.Add(new Line(p.GetPoint(), top2.GetPoint()));
                    }
                    else
                    {
                        S.Push(top);
                        S.Push(p);
                        ++i;
                    }
                }
                else
                {
                    if (p.Type == ChainPoint.PointType.Min)
                    {
                        S.Pop();
                    }

                    while (S.Count > 1)
                    {
                        var top2 = S.Pop();
                        OutLines.Add(new Line(p.GetPoint(), top2.GetPoint()));
                    }

                    S.Pop();
                    S.Push(top);
                    S.Push(p);
                    ++i;
                }
            }
        }
        public class ChainPoint : Point
        {
            public enum PointType
            {
                LeftChain,
                RightChain,
                Min,
                Max
            }

            public PointType Type { get; set; }
            public ChainPoint(Point p, PointType pType)
                : base(p.X, p.Y)
            {
                Type = pType;
            }

            public Point GetPoint()
            {
                return new Point(X, Y);
            }

            public override string ToString()
            {
                return "(" + X + ", " + Y + ") => " + Type;
            }
        }


        public bool Ccw(List<Point> points)
        {
            for (var i = 0; i < points.Count - 3; ++i)
            {
                if (HelperMethods.CheckTurn(new Line(points[i], points[i + 1]), points[i + 2]) != Enums.TurnType.Left)
                    return false;
            }

            return true;
        }

        public bool IsConvex(ChainPoint a, ChainPoint b, ChainPoint c)
        {
            return (HelperMethods.CheckTurn(new Line(a, b), c) == Enums.TurnType.Left && b.Type == ChainPoint.PointType.LeftChain) ||
                      (HelperMethods.CheckTurn(new Line(a, b), c) == Enums.TurnType.Right && b.Type == ChainPoint.PointType.RightChain);
        }

        public bool CheckMonotone(List<Point> points)
        {
            var minYIndex = points.FindIndex(p => p.Y == points.Min(e => e.Y));
            var maxYIndex = points.FindIndex(p => p.Y == points.Max(e => e.Y));
            var n = points.Count;

            for (var i = Next(maxYIndex, n); i != minYIndex; i = Next(i, n))
            {
                if (points[i].Y > points[Prev(i, n)].Y)
                    return false;
            }

            for (var i = Prev(maxYIndex, n); i != minYIndex; i = Prev(i, n))
            {
                if (points[i].Y > points[Next(i, n)].Y)
                    return false;
            }

            return true;
        }

        public List<ChainPoint> PreparePoints(List<Point> points)
        {
            var newPoints = new List<ChainPoint>();

            var minYIndex = points.FindIndex(p => p.Y == points.Min(e => e.Y));
            var maxYIndex = points.FindIndex(p => p.Y == points.Max(e => e.Y));
            var n = points.Count;

            newPoints.Add(new ChainPoint(points[minYIndex], ChainPoint.PointType.Min));
            newPoints.Add(new ChainPoint(points[maxYIndex], ChainPoint.PointType.Max));

            for (var i = Next(maxYIndex, n); i != minYIndex; i = Next(i, n))
            {
                newPoints.Add(new ChainPoint(points[i], ChainPoint.PointType.LeftChain));
            }

            for (var i = Prev(maxYIndex, n); i != minYIndex; i = Prev(i, n))
            {
                newPoints.Add(new ChainPoint(points[i], ChainPoint.PointType.RightChain));
            }

            return newPoints.OrderByDescending(e => e.Y).ThenByDescending(e => e.X).ToList();
        }

        public int Next(int i, int n)
        {
            return (i + 1) % n;
        }

        public int Prev(int i, int n)
        {
            return ((i - 1 >= 0) ? (i - 1) : (n - 1));
            //return (i - 1 + n) % n;
        }

        public override string ToString()
        {
            return "Monotone Triangulation";
        }
    }
}
