﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGUtilities;
using CGUtilities.DataStructures;

namespace CGAlgorithms.Algorithms.SegmentIntersection
{
    public class SweepLine:Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            Initialize(lines);
            while (Q.Count > 0)
            {
                var currentEventPoint = Q.GetFirst();
                Q.RemoveFirst();
                HandleEvent(currentEventPoint);
            }

            outPoints.AddRange(OutPutPoints.GetRange(0, OutPutPoints.Count));
        }

        public List<Segment> S;
        public List<Point> OutPutPoints;
        public OrderedSet<Segment> L;
        public OrderedSet<EventPoint> Q;
        public OrderedSet<EventPoint> T;

        public void Initialize(List<Line> lines)
        {
            OutPutPoints = new List<Point>();
            L = new OrderedSet<Segment>();
            Q = new OrderedSet<EventPoint>();
            S = new List<Segment>();
            T = new OrderedSet<EventPoint>();

            for (var i = 0; i < lines.Count; ++i)
            {
                var currentLine = lines[i];

                // Add new Segment
                S.Add(currentLine.Start.X < currentLine.End.X
                    ? new Segment(currentLine.Start, currentLine.End, i)
                    : new Segment(currentLine.End, currentLine.Start, i));

                // Add both endpoints of current Segment
                Q.Add(new EventPoint(S[i].Start, EventPoint.EventType.Start, i));
                Q.Add(new EventPoint(S[i].End, EventPoint.EventType.End, i));
            }
        }
        public void HandleEvent(EventPoint currentEvent)
        {
            //MessageBox.Show(currentEvent.ToString());
            // Key = lower, Value = upper
            switch (currentEvent.Type)
            {
                case EventPoint.EventType.Start:
                    {
                        /*
                         * Insert current event in L.
                         * CheckIntersection( L.prev(currentSegment), currentSegment).
                         * CheckIntersection( currentSegment, L.next(currentSegment)).
                         */

                        var segment = S[currentEvent.FirstSegmentIndex];

                        L.Add(segment);

                        // Key is the next, value is the previous.
                        //var nextPrev = L.DirectUpperAndLower(segment);
                        var nextPrev = L.DirectRightAndLeft(segment);

                        if (nextPrev.Value != null) // Upper, previous
                            CheckIntersection(nextPrev.Value.SegmentIndex, currentEvent.FirstSegmentIndex);

                        if (nextPrev.Key != null)// Lower, next
                            CheckIntersection(currentEvent.FirstSegmentIndex, nextPrev.Key.SegmentIndex);
                    }
                    break;

                case EventPoint.EventType.End:
                    {

                        //var nextPrev = L.DirectUpperAndLower(currentEvent);
                        /*
                        var nextPrev =
                            L.DirectUpperAndLower(L.FindAll(e => e.SegmentIndex == currentEvent.FirstSegmentIndex).First());
                        */
                       
                        //var nextPrev = L.DirectUpperAndLower(S[currentEvent.FirstSegmentIndex]);
                        var nextPrev = L.DirectRightAndLeft(S[currentEvent.FirstSegmentIndex]);

                        if (nextPrev.Key != null && nextPrev.Value != null)
                            //CheckIntersection(S[nextPrev.Value.FirstSegmentIndex], S[nextPrev.Key.FirstSegmentIndex]);
                            CheckIntersection(nextPrev.Value.SegmentIndex, nextPrev.Key.SegmentIndex);

                        // Delete Event belongs to the same segment (segment inex is unique).
                        L.RemoveAll(e => e.SegmentIndex == currentEvent.FirstSegmentIndex);
                    }
                    break;

                case EventPoint.EventType.Intersection:
                    {
                        /*
                        var s1 = L.FindAll(e => e.SegmentIndex == currentEvent.FirstSegmentIndex).First();
                        var s2 = L.FindAll(e => e.SegmentIndex == currentEvent.SecondSegmentIndex).First();
                        */

                        var s1 = S[currentEvent.FirstSegmentIndex];
                        var s2 = S[currentEvent.SecondSegmentIndex];

                        ////////////////////////////
                        /*
                        var s1Prev = L.DirectUpperAndLower(s1).Value;
                        var s2Next = L.DirectUpperAndLower(s2).Key;
                        
                        if (s1Prev != null)
                            CheckIntersection(s1Prev.SegmentIndex, s2.SegmentIndex);
                        if (s2Next != null)
                            CheckIntersection(s2Next.SegmentIndex, s1.SegmentIndex);
                        */
                        //var s1Next = L.DirectUpperAndLower(s1).Key;
                        var s1Next = L.DirectRightAndLeft(s1).Key;
                        //var s2Prev = L.DirectUpperAndLower(s2).Value;
                        var s2Prev = L.DirectRightAndLeft(s2).Value;

                        if (s1Next != null)
                        {
                            CheckIntersection(s1Next.SegmentIndex, s2.SegmentIndex);
                        }
                        if (s2Prev != null)
                        {
                            CheckIntersection(s2Prev.SegmentIndex, s1.SegmentIndex);
                        }

                        L.Remove(s1);
                        L.Remove(s2);


                        // change start point of both segments to intersection point
                        s1.Start = currentEvent;
                        //S[s1.SegmentIndex].Start = currentEvent;
                        s2.Start = currentEvent;
                        //S[s2.SegmentIndex].Start = currentEvent;

                        L.Add(s1);
                        L.Add(s2);
                    }
                    break;

                // No Default case.
            }
        }
        public void CheckIntersection(int i, int j)
        {
            var l1 = S[i];
            var l2 = S[j];

            if ((HelperMethods.CheckTurn(l1, l2.Start) != HelperMethods.CheckTurn(l1, l2.End)) &&
                (HelperMethods.CheckTurn(l2, l1.Start) != HelperMethods.CheckTurn(l2, l1.End)))
            {
                // We have Intersection.
                /*
                 * https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
                 * 
                 * x = (d - c)/(a - b)
                 * y = a[(d - c)/(a - b)] + c
                 * 
                 *  y=ax+c and y=bx+d where a and b are the slopes (gradients) of the lines and where c and d are the y-intercepts of the 
                 */

                // a, c => l1
                // b, d => l2
                // y − y1 = m(x − x1) 

                var a = (l1.Start.Y - l1.End.Y) / (l1.Start.X - l1.End.X);
                var c = l1.Start.Y - (a * l1.Start.X);

                var b = (l2.Start.Y - l2.End.Y) / (l2.Start.X - l2.End.X);
                var d = l2.Start.Y - (b * l2.Start.X);

                var x = (d - c) / (a - b);
                var y = ((a * (x)) + c);
                var p = new Point(x, y);


                var intersectionPoint = (l1.Start.Y < l2.Start.Y) ?
                                        (new EventPoint(p, i, j)) :
                                        (new EventPoint(p, j, i));

                if (T.Contains(intersectionPoint)) return;

                Q.Add(intersectionPoint);
                OutPutPoints.Add(p);
                T.Add(intersectionPoint);
            }
        }
        public override string ToString()
        {
            return "Sweep Line 2";
        }
        public static bool SameValue(double a, double b)
        {
            return Math.Abs(a - b) <= Constants.Epsilon;
        }
        public class EventPoint : Point, IComparable<EventPoint>
        {
            public enum EventType
            {
                Start,
                End,
                Intersection
            }

            public Point GetPoint()
            {
                return new Point(X, Y);
            }
            public EventType Type { get; set; }
            public int FirstSegmentIndex { get; set; }
            public int SecondSegmentIndex { get; set; }

            public EventPoint(Point p, EventType eventType, int segmentIndex)
                : base(p.X, p.Y)
            {
                Type = eventType;
                FirstSegmentIndex = segmentIndex;
            }

            public EventPoint(Point p, int firstSegmentIndex, int secondSegmentIndex) :
                this(p, EventType.Intersection, firstSegmentIndex)
            {
                SecondSegmentIndex = secondSegmentIndex;
            }

            public int CompareTo(EventPoint other)
            {
                if (SameValue(X, other.X))
                {
                    if (Y > other.Y)
                        return -1;
                    return Y < other.Y ? 1 : 0;
                }

                if (X < other.X)
                    return -1;

                return X > other.X ? 1 : 0;
            }

            public override string ToString()
            {
                return "(" + X + ", " + Y + ") => " + Type;
            }
        }// End EvenPoint 
        public class Segment : Line, IComparable<Segment>
        {
            public int SegmentIndex { get; set; }

            public Segment(Point startPoint, Point endPoint, int segmentIndex)
                : base(startPoint, endPoint)
            {
                SegmentIndex = segmentIndex;
            }

            public int CompareTo(Segment other)
            {
                if (Start.Equals(other.Start))
                {
                    // start point is shared
                    if (SameValue(End.Y, other.End.Y))
                    {
                        if (End.X < other.End.X)
                            return -1;
                        if (End.X > other.End.X)
                            return 1;
                    }
                    else
                    {

                        if (End.Y > other.End.Y)
                            return -1;
                        if (End.Y < other.End.Y)
                            return 1;
                    }
                }
                else
                {
                    // Different start points
                    if (SameValue(Start.Y, other.Start.Y))
                    {
                        if (Start.X < other.Start.X)
                            return -1;
                        if (Start.X > other.Start.X)
                            return 1;
                    }
                    else
                    {

                        if (Start.Y > other.Start.Y)
                            return -1;
                        if (Start.Y < other.Start.Y)
                            return 1;
                    }
                }

                // Assuming no duplicated edges.
                return 0;
            }

            public override string ToString()
            {
                return "(" + Start.X + ", " + Start.Y + ") | (" + End.X + ", " + End.Y + ").";
            }

            public override bool Equals(object obj)
            {
                var other = (Segment)obj;

                return Start.Equals(other.Start) && End.Equals(other.End);
            }
        } // end Segment

    }// End Sweepline2

}// End namespace

