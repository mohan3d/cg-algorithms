﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class ExtremeSegments : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {

            points = RemoveCollinears(points);
            if (HelperMethods.ThreePointsOrFewer(points, ref outPoints)) return;

            foreach (Point i in points)
            {
                foreach (Point j in points)
                {
                    if (i.Equals(j))
                        continue;

                    Line l = new Line(i, j);
                    bool firstTime = true;
                    bool accepted = true;
                    // Just not to be null or undefined
                    Enums.TurnType turnType = Enums.TurnType.Colinear;

                    foreach (Point k in points)
                    {
                        if (k.Equals(i) || k.Equals(j))
                            continue;
                        
                        if (firstTime)
                        {
                            turnType = HelperMethods.CheckTurn(l, k);
                            firstTime = false;
                            continue;
                        }// end firsttime

                        if (HelperMethods.CheckTurn(l, k) != turnType)
                        {
                            accepted = false;
                            break;
                        }// end accepted 
                    }// end k

                    if (accepted)
                    {
                        outPoints.Add(l.Start);
                        outPoints.Add(l.End);
                    }
                }// end j
            }// ednd i

            outPoints = HelperMethods.UniqueList(outPoints);
        }

        public List<Point> RemoveCollinears(List<Point> points)
        {
            // O(n^3)
            var retPoints = new HashSet<Point>();
            //retPoints.AddRange(points);
            points.ForEach(p => retPoints.Add(p));
            foreach (var i in points)
            {
                foreach (var j in points)
                {
                    if(i.Equals(j)) continue;

                    foreach (var k in points)
                    {
                        if(i.Equals(k) || j.Equals(k)) continue;

                        if (HelperMethods.PointOnSegment(k, i, j))
                            retPoints.Remove(k);
                    }
                }
            }

            return retPoints.ToList();
        }
        public override string ToString()
        {
            return "Convex Hull - Extreme Segments";
        }
    }
}