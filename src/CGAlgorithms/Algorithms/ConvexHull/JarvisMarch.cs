﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class JarvisMarch : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            points = HelperMethods.UniqueList(points);
            Point minXp = points.Find(s => s.Y == points.Min(e => e.Y));

            int minYPointIndex = points.IndexOf(minXp);
            int p = minYPointIndex;
            int q;

            do
            {                
                q = HelperMethods.next(p, points.Count);
                for (int i = 0; i < points.Count; i++)
                {
                    if (HelperMethods.CheckTurn(new Line(points[p], points[i]), points[q]) == Enums.TurnType.Left)
                        q = i;

                    else if (HelperMethods.CheckTurn(new Line(points[p], points[q]), points[i]) == Enums.TurnType.Colinear &&
                        points[p].Vector(points[i]).Magnitude() > points[p].Vector(points[q]).Magnitude())
                        q = i;
                }

                outPoints.Add(points[q]);
                p = q;

            } while (p != minYPointIndex);
        }

        public override string ToString()
        {
            return "Convex Hull - Jarvis March";
        }
    }
}
