﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class ExtremePoints : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            points = HelperMethods.UniqueList(points);

            for (int i = 0; i < points.Count; ++i)
            {
                if (accepted(points, i))
                {
                    outPoints.Add(points[i]);
                }
            }
        }

        private bool accepted(List<Point> points, int i)
        {
            for (int j = 0; j < points.Count; ++j)
            {
                if (i == j) continue;

                for (int k = 0; k < points.Count; ++k)
                {
                    if (i == k || k == j) continue;

                    for (int l = 0; l < points.Count; ++l)
                    {
                        if (i == l || j == l || k == l) continue;
                        if (HelperMethods.PointOnSegment(points[i], points[j], points[l])) return false;
                        if (HelperMethods.PointInTriangle(points[i], points[j], points[k], points[l]) ==
                            Enums.PointInPolygon.Inside)
                            return false;
                    }
                }
            }

            return true;
        }

        public override string ToString()
        {
            return "Convex Hull - Extreme Points";
        }
    }
}
