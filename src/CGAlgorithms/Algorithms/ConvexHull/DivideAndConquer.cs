﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class DivideAndConquer : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            points = HelperMethods.UniqueList(points);
            List<Point> pp = points.OrderBy(p => p.X).ThenBy(p => p.Y).ToList();
            outPoints.AddRange(Divide(pp));
        }

        private List<Point> Divide(List<Point> points)
        {
           
            if (points.Count <= 3)
            {
                if (points.Count <= 2 || HelperMethods.CheckTurn(new Line(points[0], points[1]), points[2]) == Enums.TurnType.Left)
                    return points;
                else if (HelperMethods.CheckTurn(new Line(points[0], points[1]), points[2]) == Enums.TurnType.Colinear)
                {
                    List<Point> newPoints = new List<Point>();
                    newPoints.Add(points[0]);
                    newPoints.Add(points[2]);
                    return newPoints;
                }
                else
                {
                    Point tmp = points[1];
                    points[1] = points[2];
                    points[2] = tmp;
                    return points;
                }
            }

            var pp = points;
            int n = pp.Count;
            List<Point> leftHull = pp.GetRange(0, n / 2);
            // Odd size
            List<Point> rightHull = pp.GetRange(n / 2, (n % 2 == 1) ? (n / 2 + 1) : (n / 2));

            return MergeHulls(Divide(leftHull), Divide(rightHull));
        }

        private List<Point> MergeHulls(List<Point> leftHull, List<Point> rightHull)
        {
            var UlhmRightIndex = -1;
            var UlhmLeftIndex = -1;

            for (var i = 0; i < leftHull.Count; ++i)
            {
                if (UlhmRightIndex == -1)
                {
                    UlhmRightIndex = i;
                    continue;
                }

                if (leftHull[UlhmRightIndex].X < leftHull[i].X)
                {
                    UlhmRightIndex = i;
                }
                else if (leftHull[UlhmRightIndex].X == leftHull[i].X/*HelperMethods.SameValue(leftHull[UlhmRightIndex].X, leftHull[i].X)*/)
                {
                    if (leftHull[UlhmRightIndex].Y < leftHull[i].Y)
                        UlhmRightIndex = i;
                }
            }

            for (var i = 0; i < rightHull.Count; ++i)
            {
                if (UlhmLeftIndex == -1)
                {
                    UlhmLeftIndex = i;
                    continue;
                }

                if (rightHull[UlhmLeftIndex].X > rightHull[i].X)
                {
                    UlhmLeftIndex = i;
                }
                else if (rightHull[UlhmLeftIndex].X == rightHull[i].X/*HelperMethods.SameValue(rightHull[UlhmLeftIndex].X, rightHull[i].X)*/)
                {
                    if (rightHull[UlhmLeftIndex].Y > rightHull[i].Y)
                        UlhmLeftIndex = i;
                }
            }


            // Upper Tangent
            while (true)
            {

                // Left hull
                while (!AllDown(leftHull, new Line(leftHull[UlhmRightIndex], rightHull[UlhmLeftIndex])))
                {

                    UlhmRightIndex = HelperMethods.next(UlhmRightIndex, leftHull.Count);
                }

                // Right hull
                while (!AllDown(rightHull, new Line(leftHull[UlhmRightIndex], rightHull[UlhmLeftIndex])))
                {

                    UlhmLeftIndex = HelperMethods.prev(UlhmLeftIndex, rightHull.Count);
                }

                if (AllDown(leftHull, new Line(leftHull[UlhmRightIndex], rightHull[UlhmLeftIndex])))
                {
                    break;
                }
            }

            int LlhmRightIndex = leftHull.FindIndex(e => e.X == leftHull.Max(p => p.X));
            int LlhmLeftIndex = rightHull.FindIndex(e => e.X == rightHull.Min(p => p.X));


            // Lower Tangent
            while (true)
            {
                // Left hull
                while (!AllUp(leftHull, new Line(leftHull[LlhmRightIndex], rightHull[LlhmLeftIndex])))
                {
                    LlhmRightIndex = HelperMethods.prev(LlhmRightIndex, leftHull.Count);
                }

                // Right hull
                while (!AllUp(rightHull, new Line(leftHull[LlhmRightIndex], rightHull[LlhmLeftIndex])))
                {
                    LlhmLeftIndex = HelperMethods.next(LlhmLeftIndex, rightHull.Count);
                }

                if (AllUp(leftHull, new Line(leftHull[LlhmRightIndex], rightHull[LlhmLeftIndex])))
                {
                    break;
                }
            }


            List<Point> retPoints = new List<Point>();
            /* Remove collinears if found */

            // Left hull.
            var n = leftHull.Count;
            var current = rightHull[UlhmLeftIndex];
            var nextp = leftHull[UlhmRightIndex];
            var nextnextp = leftHull[HelperMethods.next(UlhmRightIndex, n)];

            if (HelperMethods.PointOnSegment(nextp, current, nextnextp))
            {
                UlhmRightIndex = HelperMethods.next(UlhmRightIndex, n);
            }

            current = rightHull[LlhmLeftIndex];
            var prevp = leftHull[LlhmRightIndex];
            var prevprevp = leftHull[HelperMethods.prev(LlhmRightIndex, n)];            

            if (HelperMethods.PointOnSegment(prevp, prevprevp, current))
            {
                LlhmRightIndex = HelperMethods.prev(LlhmRightIndex, n);
            }

            // Right hull.
            n = rightHull.Count;
            current = leftHull[UlhmRightIndex];
            nextp = rightHull[UlhmLeftIndex];
            nextnextp = rightHull[HelperMethods.prev(UlhmLeftIndex, n)];

            if (HelperMethods.PointOnSegment(nextp, current, nextnextp))
            {
                UlhmLeftIndex = HelperMethods.prev(UlhmLeftIndex, n);
            }
            // Edit.
            prevp = rightHull[LlhmLeftIndex];
            prevprevp = rightHull[HelperMethods.next(LlhmLeftIndex, n)];
            current = leftHull[LlhmRightIndex];

            if (HelperMethods.PointOnSegment(prevp, prevprevp, current))
            {
                LlhmLeftIndex = HelperMethods.next(LlhmLeftIndex, n);
            }

            for (int i = UlhmRightIndex;
                     i != LlhmRightIndex/*next(LlhmRightIndex, leftHull.Count)*/;
                     i = HelperMethods.next(i, leftHull.Count))
            {
                retPoints.Add(leftHull[i]);
            }
            retPoints.Add(leftHull[LlhmRightIndex]);

            for (int i = LlhmLeftIndex;
                     i != UlhmLeftIndex/*next(UlhmLeftIndex, rightHull.Count)*/;
                     i = HelperMethods.next(i, rightHull.Count))
            {
                retPoints.Add(rightHull[i]);
            }
            retPoints.Add(rightHull[UlhmLeftIndex]);


            return retPoints;
        }
                
        
        private bool AllUp(List<Point> points, Line l)
        {
            List<Point> cPoints = new List<Point>(points);
            cPoints.Remove(points.Find(p => p.Equals(l.Start)));
            cPoints.Remove(points.Find(p => p.Equals(l.End)));


            var pts = cPoints.ConvertAll(p => HelperMethods.CheckTurn(l, p));
            return pts.TrueForAll(p => p != Enums.TurnType.Right) || pts.Count == 0;
        }

        private bool AllDown(List<Point> points, Line l)
        {
            List<Point> cPoints = new List<Point>(points);
            cPoints.Remove(points.Find(p => p.Equals(l.Start)));
            cPoints.Remove(points.Find(p => p.Equals(l.End)));


            var pts = cPoints.ConvertAll(p => HelperMethods.CheckTurn(l, p));
            return pts.TrueForAll(p => p != Enums.TurnType.Left) || pts.Count == 0;
        }


        public override string ToString()
        {
            return "Convex Hull - Divide & Conquer";
        }
    }
}
