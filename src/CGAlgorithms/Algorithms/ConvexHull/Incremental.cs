﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class Incremental : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            points = HelperMethods.UniqueList(points);
            List<Point> ppoints = points.OrderBy(e => e.X).ThenBy(p => p.Y).ToList();
            List<Point> tmpPoints = new List<Point>();

            if (HelperMethods.ThreePointsOrFewer(ppoints, ref outPoints)) return;
          
            // Incremental algorithm must start with a triangle !
            while (ppoints.Count >= 3 && HelperMethods.CheckTurn(new Line(ppoints[0], ppoints[1]), ppoints[2]) == Enums.TurnType.Colinear)
            {
                ppoints.RemoveAt(1);
            }

            if (HelperMethods.ThreePointsOrFewer(ppoints, ref outPoints)) return;

            if (HelperMethods.CheckTurn(new Line(ppoints[0], ppoints[1]), ppoints[2]) == Enums.TurnType.Left)
            {
                tmpPoints.AddRange(ppoints.GetRange(0, 3));
            }
            else
            {
                tmpPoints.Add(ppoints[0]);
                tmpPoints.Add(ppoints[2]);
                tmpPoints.Add(ppoints[1]);
            }

            // > 3 points
            // tmpPoints.AddRange(outPoints.GetRange(0, 3));
            for (int i = 3; i < ppoints.Count; ++i)
            {
                if (!InsidePolygon(tmpPoints, ppoints[i]))
                {
                    List<Point> rightHull = new List<Point>();
                    rightHull.Add(ppoints[i]);

                    tmpPoints = MergeHulls(tmpPoints, rightHull);
                }
            }

            outPoints.Clear();
            outPoints.AddRange(tmpPoints.GetRange(0, tmpPoints.Count));
        }


        private List<Point> MergeHulls(List<Point> leftHull, List<Point> rightHull)
        {
            int UlhmRightIndex = leftHull.FindIndex(e => e.X == leftHull.Max(p => p.X));
            int UlhmLeftIndex = rightHull.FindIndex(e => e.X == rightHull.Min(p => p.X));

            // Upper Tangent
            while (true)
            {

                // Left hull
                while (!AllDown(leftHull, new Line(leftHull[UlhmRightIndex], rightHull[UlhmLeftIndex])))
                {

                    UlhmRightIndex = HelperMethods.next(UlhmRightIndex, leftHull.Count);
                }

                // Right hull
                while (!AllDown(rightHull, new Line(leftHull[UlhmRightIndex], rightHull[UlhmLeftIndex])))
                {

                    UlhmLeftIndex = HelperMethods.prev(UlhmLeftIndex, rightHull.Count);
                }

                if (AllDown(leftHull, new Line(leftHull[UlhmRightIndex], rightHull[UlhmLeftIndex])))
                {
                    break;
                }
            }

            int LlhmRightIndex = leftHull.FindIndex(e => e.X == leftHull.Max(p => p.X));
            int LlhmLeftIndex = rightHull.FindIndex(e => e.X == rightHull.Min(p => p.X));


            // Lower Tangent
            while (true)
            {
                // Left hull
                while (!AllUp(leftHull, new Line(leftHull[LlhmRightIndex], rightHull[LlhmLeftIndex])))
                {
                    LlhmRightIndex = HelperMethods.prev(LlhmRightIndex, leftHull.Count);
                }

                // Right hull
                while (!AllUp(rightHull, new Line(leftHull[LlhmRightIndex], rightHull[LlhmLeftIndex])))
                {
                    LlhmLeftIndex = HelperMethods.next(LlhmLeftIndex, rightHull.Count);
                }

                if (AllUp(leftHull, new Line(leftHull[LlhmRightIndex], rightHull[LlhmLeftIndex])))
                {
                    break;
                }
            }


            List<Point> retPoints = new List<Point>();

            var n = leftHull.Count;
            var current = rightHull[0];
            var nextp = leftHull[UlhmRightIndex];
            var nextnextp = leftHull[HelperMethods.next(UlhmRightIndex, n)];
            if (HelperMethods.CheckTurn(new Line(current, nextp), nextnextp) == Enums.TurnType.Colinear)
            {
                UlhmRightIndex = HelperMethods.next(UlhmRightIndex, n);
            }

            var prevp = leftHull[LlhmRightIndex];
            var prevprevp = leftHull[HelperMethods.prev(LlhmRightIndex, n)];
            if (HelperMethods.CheckTurn(new Line(prevprevp, current), prevp) == Enums.TurnType.Colinear)
            {
                LlhmRightIndex = HelperMethods.prev(LlhmRightIndex, n);
            }

            for (int i = UlhmRightIndex;
                     i != LlhmRightIndex/*next(LlhmRightIndex, leftHull.Count)*/;
                     i = HelperMethods.next(i, leftHull.Count))
            {
                retPoints.Add(leftHull[i]);
            }
            retPoints.Add(leftHull[LlhmRightIndex]);

            for (int i = LlhmLeftIndex;
                     i != UlhmLeftIndex/*next(UlhmLeftIndex, rightHull.Count)*/;
                     i = HelperMethods.next(i, rightHull.Count))
            {
                retPoints.Add(rightHull[i]);
            }


            retPoints.Add(rightHull[UlhmLeftIndex]);


            return retPoints;
        }


        private bool AllUp(List<Point> points, Line l)
        {
            List<Point> cPoints = new List<Point>(points);
            cPoints.Remove(points.Find(p => p.Equals(l.Start)));
            cPoints.Remove(points.Find(p => p.Equals(l.End)));


            var pts = cPoints.ConvertAll(p => HelperMethods.CheckTurn(l, p));
            return pts.TrueForAll(p => p != Enums.TurnType.Right) || pts.Count == 0;
        }

        private bool AllDown(List<Point> points, Line l)
        {
            List<Point> cPoints = new List<Point>(points);
            cPoints.Remove(points.Find(p => p.Equals(l.Start)));
            cPoints.Remove(points.Find(p => p.Equals(l.End)));


            var pts = cPoints.ConvertAll(p => HelperMethods.CheckTurn(l, p));
            return pts.TrueForAll(p => p != Enums.TurnType.Left) || pts.Count == 0;
        }

        private bool InsidePolygon(List<Point> points, Point p)
        {
            bool firstTime = true;
            Enums.TurnType tmpType = Enums.TurnType.Colinear;

            for (int i = 1; i < points.Count; ++i)
            {
                if (firstTime)
                {
                    firstTime = false;
                    tmpType = HelperMethods.CheckTurn(new Line(points[i - 1], points[i]), p);
                }

                if (HelperMethods.CheckTurn(new Line(points[i - 1], points[i]), p) != tmpType)
                {
                    return false;
                }
            }

            if (HelperMethods.CheckTurn(new Line(points.Last(), points.First()), p) != tmpType)
            {
                return false;
            }

            return true;
        }

        public override string ToString()
        {
            return "Convex Hull - Incremental";
        }
    }
}
