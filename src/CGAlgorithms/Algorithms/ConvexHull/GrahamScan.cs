﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class GrahamScan : Algorithm
    {
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            points = HelperMethods.UniqueList(points);
            if (HelperMethods.ThreePointsOrFewer(points, ref outPoints)) return;
            
            Point minYPoint = null;
            var index = -1;

            for (var j = 0; j < points.Count; ++j)
            {
                if (minYPoint == null)
                {
                    minYPoint = points[j];
                    index = j;
                }

                if (minYPoint.Y == points[j].Y)
                {
                    if (points[j].X < minYPoint.X)
                    {
                        minYPoint = points[j];
                        index = j;
                    }
                }

                if (points[j].Y < minYPoint.Y)
                {
                    minYPoint = points[j];
                    index = j;
                }
            }
            
            //points.Remove(minYPoint);
            points.RemoveAt(index);

            List<Point> ppoints = points.OrderBy(p => Math.Atan2(p.Y - minYPoint.Y, p.X - minYPoint.X)).ToList();

            outPoints.Add(minYPoint);
            outPoints.Add(ppoints[0]);

            Point last;
            Point prevLast;

            int i = 1;

            while (i < ppoints.Count)
            {
                last = outPoints.Last();

                
                if (last.Equals(minYPoint))
                {
                    outPoints.Add(ppoints[i]);
                    ++i;
                    continue;
                }

                prevLast = outPoints[outPoints.Count - 2];

                if (HelperMethods.PointOnSegment(ppoints[i], prevLast, last))
                {
                    ++i; 
                    continue;
                }

                if (HelperMethods.PointOnSegment(last, prevLast, ppoints[i]))
                {
                    outPoints.Remove(last);
                    continue;
                }

                if (HelperMethods.CheckTurn(new Line(prevLast, last), ppoints[i]) == Enums.TurnType.Left)
                {
                    outPoints.Add(ppoints[i]);
                    ++i;
                }
                else
                {
                    outPoints.Remove(last);
                }
            }

            
            if (outPoints.Count > 2 && HelperMethods.CheckTurn(new Line(outPoints[outPoints.Count - 2], outPoints.Last()), minYPoint) != Enums.TurnType.Left)
            {
                outPoints.Remove(outPoints.Last());
            }
        }

        public override string ToString()
        {
            return "Convex Hull - Graham Scan";
        }
    }
}