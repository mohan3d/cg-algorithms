﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Threading;
using CGUtilities;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public abstract class AQuickHull : Algorithm
    {        
        public override void Run(List<Point> points, List<Line> lines, List<Polygon> polygons, ref List<Point> outPoints, ref List<Line> outLines, ref List<Polygon> outPolygons)
        {
            points = HelperMethods.UniqueList(points);
            if (HelperMethods.ThreePointsOrFewer(points, ref outPoints)) return;

            InitializeConvexHull();

            Point A = points.Find(e => e.X == points.Min(p => p.X)); // Min x
            Point B = points.Find(e => e.X == points.Max(p => p.X)); // Max x

            Line l = new Line(A, B);

            AddToConvexHull(A);
            AddToConvexHull(B);

            points.Remove(A);
            points.Remove(B);


            List<Point> S1 = points.Where(p => HelperMethods.CheckTurn(l, p) == Enums.TurnType.Right).ToList();
            List<Point> S2 = points.Where(p => HelperMethods.CheckTurn(l, p) == Enums.TurnType.Left).ToList();

            if (((S1 == null) && (S2 == null)) || (S1.Count == 0 && S2.Count == 0))
            {
                outPoints.AddRange(GetConvexHull());
                return;
            }

            CalculateConvexHull(S1, S2, A, B);
            outPoints.AddRange(GetConvexHull());
        }


        protected void FindHull(List<Point> Sk, Point P, Point Q)
        {
            if (Sk.Count <= 0)
                return;

            Point C = Sk.Aggregate((a, b) =>
                                   (0.5 * Math.Abs(((P.X - a.X) * (Q.Y - P.Y)) - ((P.X - Q.X) * (a.Y - P.Y))))
                                   > (0.5 * Math.Abs(((P.X - b.X) * (Q.Y - P.Y)) - ((P.X - Q.X) * (b.Y - P.Y))))
                                   ? a
                                   : b);

            AddToConvexHull(C);

            List<Point> S1 = Sk.Where(e => HelperMethods.CheckTurn(new Line(P, C), e) == Enums.TurnType.Right).ToList();
            List<Point> S2 = Sk.Where(e => HelperMethods.CheckTurn(new Line(C, Q), e) == Enums.TurnType.Right).ToList();

            FindHull(S1, P, C);
            FindHull(S2, C, Q);
        }
        
        
        protected abstract void AddToConvexHull(Point p);
        protected abstract void InitializeConvexHull();
        protected abstract List<Point> GetConvexHull();
        protected abstract void CalculateConvexHull(List<Point> S1, List<Point> S2, Point A, Point B);
    }
}
