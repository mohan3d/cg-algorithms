﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGUtilities;
using System.Threading;
using System.Collections.Concurrent;

namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class QuickHullThreaded : AQuickHull
    {
        private ConcurrentBag<Point> ConvexHull;

        protected override void AddToConvexHull(Point p)
        {
            ConvexHull.Add(p);
        }

        protected override void CalculateConvexHull(List<Point> S1, List<Point> S2, Point A, Point B)
        {
            Thread t1 = new Thread(() => FindHull(S1, A, B));
            Thread t2 = new Thread(() => FindHull(S2, B, A));
            t1.Start(); t2.Start();
            t1.Join();  t2.Join();
        }

        protected override List<Point> GetConvexHull()
        {
            return ConvexHull.ToList();
        }

        protected override void InitializeConvexHull()
        {
            ConvexHull = new ConcurrentBag<Point>();
        }

        public override string ToString()
        {
            return "Convex Hull - Quick Hull Threaded";
        }

    }
}
