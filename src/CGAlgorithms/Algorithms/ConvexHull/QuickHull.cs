﻿using CGUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CGAlgorithms.Algorithms.ConvexHull
{
    public class QuickHull : AQuickHull
    {        
        private List<Point> ConvexHull;

        protected override void AddToConvexHull(Point p)
        {
            ConvexHull.Add(p);
        }

        protected override void CalculateConvexHull(List<Point> S1, List<Point> S2, Point A, Point B)
        {
            FindHull(S1, A, B);
            FindHull(S2, B, A);
        }

        protected override List<Point> GetConvexHull()
        {
            return ConvexHull;
        }

        protected override void InitializeConvexHull()
        {
            ConvexHull = new List<Point>();
        }

        public override string ToString()
        {
            return "Convex Hull - Quick Hull";
        }
    }
}